### Prerequisites

* Basic knowledge of Computer Programming

### Course Content

#### Day 1

* Setting up Java
* Writing your First Java Program
* Java Access Modifiers
* Java syntax familiarization for basic programming concepts, like if, if...else, nested if...else, switch...case, for, while, do...while, etc.
* Choosing an IDE - Eclipse

#### Day 2

* Data Types and Operators
* Object Oriented Concepts - Class and Object
* Inheritance, Polymorphism and Encapsulation
* Interface and Abstract Class
* Tips on getting acquainted with Eclipse IDE

#### Day 3

* String Manipulation
* Handling Arrays
* Collections Framework
* Annotation and Enum

#### Day 4

* Exception Handling
* Thread Handling
* Reflection API

#### Day 5

* Mini project implementation